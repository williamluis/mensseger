<?php
include_once 'config.php';
include_once 'functions.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel='stylesheet' href='css/normalize.min.css' type='text/css' media='all' />
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css" >

	<style type="text/css" media="screen">
		#myTabContent{
			border: 1px solid #ddd;
    		margin-top: -1px;
    		border-bottom-left-radius: 5px;
    		border-bottom-right-radius: 5px;
    		padding: 10px 25px;
    	}

    	.red{color:red !important;}

		.clear{clear: both;}
    	.space{height: 20px;}
    	.margin-rigth{margin-right: 20px;}
    	#caixaMensagens .row {

		    border-bottom: 1px solid #ddd;
		    margin-top: 10px;
		    padding-bottom: 10px;

		}

		.ajuste_btn_alterar{
			margin-right: 10px;
    		margin-top: -3px;
		}

		.row:last-child {border-bottom:  none; }
	</style>
</head>

<body>

	<div class="container">
		<h1>Messeger</h1>
		<ul class="nav nav-tabs">

		  <li role="presentation" class="active"><a href="#listaMensagem" role="tab" id="listaMensagem-tab" data-toggle="tab" aria-controls="listaMensagem" aria-expanded="true">Lista de mensagens</a></li>
		  <li role="presentation"><a href="#listaUsuario" role="tab" id="listaUsuario-tab" data-toggle="tab" aria-controls="listaUsuario" aria-expanded="true">Lista de usuários</a></li>
		</ul>

		<div class="tab-content" id="myTabContent">

			<div class="tab-pane fade active in" role="tabpanel" id="listaMensagem" aria-labelledby="listaMensagem-tab">
				<h3>Lista de Mensagens</h3>
				<div id="caixaMensagens">
				<?php

$sql = "SELECT m.mensagem,
			   m.data_envio,
			   u.nome

				FROM tbmensagens m

				inner join tbusuarios u on u.id = m.id_usuario ";
$query = $conexao->query($sql);

while ($dados = mysqli_fetch_array($query)) {?>

		<div class="row">
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><?php echo $dados['nome'] ?></div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right"><strong>Data de envio:</strong> <?php echo formatarData($dados['data_envio']) ?></div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<strong>Mensagem:</strong> <br/><?php echo $dados['mensagem'] ?>
			</div>
		</div>

<?php }?>
			</div>
			</div>

			<div class="tab-pane fade " role="tabpanel" id="listaUsuario" aria-labelledby="listaUsuario-tab">
				<a id="deletarUsuario" class="btn btn-danger pull-right"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
				<a href="" class="btn btn-info pull-right margin-rigth" data-toggle="modal" data-target="#modalUsuario">Novo usuário</a>

				<h3>Lista de Usuarios</h3>
				<div class="space"></div>
				<div class="clear"></div>
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

					<?php

$sql = "SELECT * FROM tbusuarios";
$query = $conexao->query($sql);

while ($dados = mysqli_fetch_array($query)) {

	?>

	<div class="panel panel-default" id="panelUsuario<?php echo $dados['id']; ?>">
	    <div class="panel-heading" role="tab" id="headingOne">
	      <h4 class="panel-title">
	        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $dados['id']; ?>" aria-expanded="true" aria-controls="collapse<?php echo $dados['id']; ?>">
	          #<?php echo $dados['id']; ?> <?php echo $dados['nome']; ?>
	        </a>

	        <input type="checkbox" name="excl[]" value="<?php echo $dados['id'] ?>" class="check pull-right" />
	        <a class="btn btn-warning btn-xs pull-right ajuste_btn_alterar alterarUsuario" data-idusuario="<?php echo $dados['id'] ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>

	      </h4>
	    </div>
	    <div id="collapse<?php echo $dados['id']; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $dados['id']; ?>">
	      <div class="panel-body">

			<div class="well">
				<h4>Enviar mensagem</h4>
				<form class="formEnviarMensagem" >
					<input type="hidden" name="id_usuario" class="id_usuario" value="<?php echo $dados['id']; ?>" />
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
							    <label for="mensagem">Mensagem</label>
							    <textarea class="form-control mensagem" name="mensagem" placeholder="Mensagem"></textarea>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-success pull-right">Enviar mensagem</button>
					<div class="clear"></div>
				</form>
			</div>

	      </div>
	    </div>
	</div>

					<?php
}
?>

				</div>

			</div>

		</div>

	</div>

	<!-- Modal -->
	<div class="modal fade" id="modalUsuario" tabindex="-1" role="dialog" aria-labelledby="modalUsuario">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="modalUsuario">Novo usuário</h4>
	      </div>

	      <form id="formUsuario">
	      	<input type="hidden" name="acao" id="acao" value='1' />
	      	<input type="hidden" name="id" id="id" value='' />
	      	<div class="modal-body">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-8 col-lg-8">
						<div class="form-group">
						    <label for="nome">Nome</label>
						    <input type="text" class="form-control" id="nome" name="nome" value="<?php echo $dados['nome']; ?>" placeholder="Nome">
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
						<div class="form-group">
						    <label for="data_nascimento">Data nascimento</label>
						    <input type="text" class="form-control" id="data_nascimento" name="data_nascimento" value="<?php echo $dados['data_nascimento']; ?>" placeholder="Data nascimento">
						</div>
					</div>


					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
						<div class="form-group">
						    <label for="cep">CEP</label>
						    <input type="text" class="form-control" id="cep" name="cep" placeholder="CEP">
						</div>
					</div>

					<div class="col-xs-8 col-sm-8 col-md-4 col-lg-4">
						<div class="form-group">
						    <label for="endereco">Endereço</label>
						    <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Endereço">
						</div>
					</div>

					<div class="col-xs-8 col-sm-8 col-md-4 col-lg-4">
						<div class="form-group">
						    <label for="numero">Número</label>
						    <input type="text" class="form-control" id="numero" name="numero" placeholder="Número">
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
						<div class="form-group">
						    <label for="bairro">Bairro</label>
						    <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Bairro">
						</div>
					</div>
					<div class="col-xs-8 col-sm-8 col-md-5 col-lg-5">
						<div class="form-group">
						    <label for="cidade">Cidade</label>
						    <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Cidade">
						</div>
					</div>

					<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
						<div class="form-group">
						    <label for="estado">Estado</label>
						    <input type="text" class="form-control" id="estado" name="estado" placeholder="Estado">
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
						    <label for="biografia">Biografia</label>
						    <textarea class="form-control" name="biografia" id="biografia" placeholder="Biografia"></textarea>
						</div>
					</div>

				</div>

	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        	<button type="submit" class="btn btn-success pull-right">Salvar</button>
	      	</div>
	      </form>
	    </div>
	  </div>

	</div>

	<div class="modal fade" id="modalNotificacao" tabindex="-1" role="dialog" aria-labelledby="modalNotificacao">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        		<span aria-hidden="true">&times;</span>
	        	</button>
	        	<h4 class="modal-title" id="modalNotificacao">Novo usuário</h4>
	      	</div>
	      	<div class="modal-body"></div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      	</div>
	    </div>
	  </div>

	</div>

</body>
</html>

<!-- Jquery -->
<script src=https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js></script>

<!-- Latest compiled and minified JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.maskedinput.js"></script>

<script>
(function ($) {
$(document).ready(function(){

	$("#cep").mask("99999-999");
	$(".telefone").mask("(99)9999-9999");
	$("#data_nascimento").mask("99/99/9999");

	$('#cep').keyup(getEndereco);

});

$('.alterarUsuario').on('click', function(event) {

	var id_usuario = $(this).data('idusuario');

	$('#id').val(id_usuario);
	$('#acao').val('2');

	jQuery.ajax({
		type: 'POST',
		url: 'functions.php',
		data: 'acao=7&id_usuario=' + id_usuario,
		cache: true,
		success: function(response){

			var retorno = JSON.parse(response);
			$('#nome').val(retorno.nome);
			$('#data_nascimento').val(retorno.data_nascimento);
			$('#cep').val(retorno.cep);
			$('#endereco').val(retorno.endereco);
			$('#numero').val(retorno.numero);
			$('#bairro').val(retorno.bairro);
			$('#cidade').val(retorno.cidade);
			$('#estado').val(retorno.estado);
			$('#biografia').val(retorno.biografia);

		}
	});


	$('#modalUsuario').modal('show');

});

$('.formEnviarMensagem').submit(function(event) {
	var id_usuario = $(this).find(".id_usuario").val();
	var mensagem = $(this).find(".mensagem").val();

	jQuery.ajax({
		type: 'POST',
		url: 'functions.php',
		data: 'acao=5&id_usuario=' + id_usuario + '&mensagem=' + mensagem,
		cache: true,
		success: function(response){

			var retorno = response.substring(0,response.length - 1);
			response = retorno;
			atualizarListaMensagens();

		}
	});

	$('#modalNotificacao .modal-title').html('Mensagem envianda');
	$('#modalNotificacao .modal-body').html('Mensagem envianda com sucesso!');
	$('#modalNotificacao').modal('show');



	$(this).find(".id_usuario").val('');
	$(this).find(".mensagem").val('');

	return false;
});

$("#formUsuario").submit(function(event) {

	var id = $("#id").val();
	var acao = $("#acao").val();
	var nome = $("#nome").val();
	var dataNascimento = $("#data_nascimento").val();
	var cep = $("#cep").val();
	var endereco = $("#endereco").val();
	var numero = $("#numero").val();
	var bairro = $("#bairro").val();
	var cidade = $("#cidade").val();
	var estado = $("#estado").val();
	var biografia = $("#biografia").val();

	jQuery.ajax({
		type: 'POST',
		url: 'functions.php',
		data: 'acao=' + acao + '&id=' + id +  '&nome=' + nome + '&data_nascimento=' + dataNascimento + '&cep=' + cep + '&endereco=' + endereco + '&numero=' + numero + '&bairro=' + bairro + '&cidade=' + cidade + '&estado=' + estado +  '&biografia=' + biografia,
		cache: true,
		success: function(response){

			var retorno = response.substring(0,response.length - 1);
			response = retorno;
			atualizarListaUsuario();

		}
	});

	if(acao == '1'){
		$('#modalUsuario').modal('hide');
		$('#modalNotificacao .modal-title').html('Novo registro');
		$('#modalNotificacao .modal-body').html(nome + ' foi cadastrado com sucesso!');
		$('#modalNotificacao').modal('show');
	}else if(acao == '2'){
		$('#modalUsuario').modal('hide');
		$('#modalNotificacao .modal-title').html('Registro atualizado');
		$('#modalNotificacao .modal-body').html(nome + ' atualizado com sucesso!');
		$('#modalNotificacao').modal('show');
	}

	$(this).find(".nome").val('');
	$(this).find(".data_nascimento").val('');
	$(this).find(".cep").val('');
	$(this).find(".endereco").val('');
	$(this).find(".numero").val('');
	$(this).find(".bairro").val('');
	$(this).find(".cidade").val('');
	$(this).find(".estado").val('');
	$(this).find(".biografia").val('');

	return false;
});

$("#deletarUsuario").click(function(event) {

	var ids=[];
	var x = 0;
	$(".check").each(function(index, el) {

		if($(this).is(':checked')){
			ids[x] = $(this).val();
			x++;
		}

	});

	jQuery.ajax({
		type: 'POST',
		url: 'functions.php',
		data: 'acao=3&ids=' + ids,
		cache: true,
		success: function(response){

			var retorno = response.substring(0,response.length - 1);
			response = retorno;
			atualizarListaUsuario();

		}
	});

	$('#modalNotificacao .modal-title').html('Registro deletado');
	$('#modalNotificacao .modal-body').html('Registro deletado com sucesso!');
	$('#modalNotificacao').modal('show');

	return false;
});

$('#listaMensagem-tab a').tab('show');
$('#listaUsuario-tab a').tab('show');

function atualizarListaUsuario(){
	jQuery.ajax({
		type: 'POST',
		url: 'functions.php',
		data: 'acao=4',
		cache: true,
		success: function(response){

			var retorno = response.substring(0,response.length - 1);
			response = retorno;
			$("#accordion").html(response);
			//alert(response);


		}
	});
}

function atualizarListaMensagens(){
	jQuery.ajax({
		type: 'POST',
		url: 'functions.php',
		data: 'acao=6',
		cache: true,
		success: function(response){

			var retorno = response.substring(0,response.length - 1);
			response = retorno;
			$("#caixaMensagens").html(response);
			//alert(response);


		}
	});
}


function getEndereco() {

		if($.trim($("#cep").val()) != ""){

			$.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+$("#cep").val(), function(){

				if (resultadoCEP["tipo_logradouro"] != '') {
					if (resultadoCEP["resultado"]) {
						// troca o valor dos elementos
						$("#endereco").val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"]));
						$("#bairro").val(unescape(resultadoCEP["bairro"]));
						$("#cidade").val(unescape(resultadoCEP["cidade"]));
						$("#estado").val(unescape(resultadoCEP["uf"]));
						$("#numero").focus();
					}
				}
			});
		}
	}

}(jQuery));
</script>