﻿# Host: localhost  (Version 5.5.5-10.1.22-MariaDB)
# Date: 2017-06-27 23:36:27
# Generator: MySQL-Front 6.0  (Build 1.180)


#
# Structure for table "tbusuarios"
#

DROP TABLE IF EXISTS `tbusuarios`;
CREATE TABLE `tbusuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `data_nascimento` varchar(255) DEFAULT NULL,
  `biografia` text,
  `data_criacao` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `cep` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `numero` varchar(255) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

#
# Data for table "tbusuarios"
#

INSERT INTO `tbusuarios` VALUES (1,'William luis','','teste','2017-06-26 22:20:58','13453-615','Rua AnalÃ¢ndia','322','Residencial SÃ£o Joaquim','Santa BÃ¡rbara DOeste','SP',NULL),(2,'Andre',NULL,NULL,'2017-06-26 22:53:45',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(33,'JoÃ£o','','','2017-06-27 16:46:49','','','','','','',NULL);

#
# Structure for table "tbmensagens"
#

DROP TABLE IF EXISTS `tbmensagens`;
CREATE TABLE `tbmensagens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `mensagem` text,
  `data_envio` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `id_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `tbusuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "tbmensagens"
#

INSERT INTO `tbmensagens` VALUES (1,1,'teste','2017-06-27 17:17:03'),(2,2,'segunda mensagem','2017-06-27 17:23:17'),(3,33,'d S FSF SDF SFSDdf sdf s\ndf sdf sdf','2017-06-27 17:24:15'),(4,2,'teste teste teste teste teste 3333','2017-06-27 23:30:29'),(5,1,'teste\nteste','2017-06-27 23:31:39');
