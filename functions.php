<?php

include_once 'config.php';

function formatarData($datatime) {

	$arrayDataTime = explode(" ", $datatime);

	$data = explode("-", $arrayDataTime[0]);

	$ano = $data[0];
	$mes = $data[1];
	$dia = $data[2];

	$return = $dia . "/" . $mes . "/" . $ano;

	return $return;

}

$acao = 0;
foreach ($_POST as $key => $value) {
	${$key} = $value;
}

if ($acao == "1") {

	$sql = "INSERT tbusuarios (nome, data_nascimento, cep, endereco, numero, bairro, cidade, estado, biografia)
    VALUES ('" . $nome . "', '" . $data_nascimento . "', '" . $cep . "', '" . $endereco . "', '" . $numero . "', '" . $bairro . "', '" . $cidade . "', '" . $estado . "', '" . $biografia . "')";

	mysqli_query($conexao, $sql);

	exit;

} elseif ($acao == '2') {

	$sql = "UPDATE tbusuarios SET nome='" . $nome . "', data_nascimento='" . $data_nascimento . "', cep='" . $cep . "', endereco='" . $endereco . "', numero='" . $numero . "', bairro='" . $bairro . "', cidade='" . $cidade . "', estado='" . $estado . "', biografia='" . $biografia . "' WHERE id=" . $id;

	mysqli_query($conexao, $sql);

	exit;

} elseif ($acao == '3') {

	$idArray = explode(',', $ids);

	foreach ($idArray as $value) {
		$sql = "DELETE FROM tbusuarios WHERE id = " . $value;
		mysqli_query($conexao, $sql);
	}

	exit;

} elseif ($acao == '4') {

	$sql = "SELECT * FROM tbusuarios";
	$query = $conexao->query($sql);

	$lista = "";
	while ($dados = mysqli_fetch_array($query)) {
		$lista .= "<div class='panel panel-default' id='panelUsuario" . $dados['id'] . "'>
	    <div class='panel-heading' role='tab' id='headingOne'>
	      <h4 class='panel-title'>
	        <a role='button' data-toggle='collapse' data-parent='#accordion' href='#collapse" . $dados['id'] . "' aria-expanded='true' aria-controls='collapse" . $dados['id'] . "'>
	          #" . $dados['id'] . " " . $dados['nome'] . "
	        </a>

	        <input type='checkbox' name='excl[]' value='" . $dados['id'] . "' class='check pull-right' />
			<a id='alterarUsuario' class='btn btn-warning btn-xs pull-right ajuste_btn_alterar'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a>
	      </h4>
	    </div>
	    <div id='collapse" . $dados['id'] . "' class='panel-collapse collapse' role='tabpanel' aria-labelledby='heading" . $dados['id'] . "'>
	      <div class='panel-body'>
				<div class='well'>
					<h4>Enviar mensagem</h4>
					<form class='formEnviarMensagem'>
						<input type='hidden' name='id_usuario' class='id_usuario' value='" . $dados['id'] . "' />
						<div class='row'>
							<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
								<div class='form-group'>
								    <label for='mensagem'>Mensagem</label>
								    <textarea class='form-control mensagem' name='mensagem' placeholder='Mensagem'></textarea>
								</div>
							</div>
						</div>
						<button type='submit' class='btn btn-success pull-right'>Enviar mensagem</button>
						<div class='clear'></div>
					</form>
				</div>
	      </div>
	    </div>
		</div>";
	}

	echo $lista;
	exit;

} elseif ($acao == '5') {
	$sql = "INSERT tbmensagens (id_usuario, mensagem)
    VALUES ('" . $id_usuario . "', '" . $mensagem . "')";

	mysqli_query($conexao, $sql);
	exit;
} elseif ($acao == '6') {

	$lista = "";
	$sql = "SELECT m.mensagem,
			   m.data_envio,
			   u.nome

				FROM tbmensagens m

				inner join tbusuarios u on u.id = m.id_usuario ";
	$query = $conexao->query($sql);

	while ($dados = mysqli_fetch_array($query)) {

		$lista .= "<div class='row'>
			<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'>" . $dados['nome'] . "</div>
			<div class='col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right'><strong>Data de envio:</strong> " . formatarData($dados['data_envio']) . "</div>
			<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
				<strong>Mensagem:</strong> <br/>" . $dados['mensagem'] . "
			</div>
		</div>";
	}

	echo $lista;
	exit;
} elseif ($acao == '7') {

	$sql = "SELECT * FROM tbusuarios where id=" . $id_usuario;
	$query = $conexao->query($sql);

	while ($dados = mysqli_fetch_array($query)) {

		$json = '{"nome":"' . $dados['nome'] . '", "data_nascimento":"' . $dados['data_nascimento'] . '", "cep":"' . $dados['cep'] . '", "endereco":"' . $dados['endereco'] . '", "numero":"' . $dados['numero'] . '", "bairro":"' . $dados['bairro'] . '", "cidade":"' . $dados['cidade'] . '", "estado":"' . $dados['estado'] . '", "biografia":"' . $dados['biografia'] . '", "telefone":"' . $dados['telefone'] . '"}';

		//$json = '{"nome":"' . $dados['nome'] . '"}';

	}

	echo $json;

	exit;
}

?>